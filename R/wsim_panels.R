#' Plot Routine WSIM Panel Graphics
#'
#' Make a Python call to mapserver for a panel figure depicting a time series
#' of a given Routine WSIM metric.
#'
#' @param wsim_graphics_path Character string with path to `wsim-graphics.py`.
#' @param roi Character string with region of interest.
#' @param data_version Numeric value in format (`YYYYMM`) for WSIM data version
#'   to use.
#' @param start_month Numeric value specifying the starting month of integration
#'   period, if different from dataversion month. The starting month is only
#'   consistent across panels when period = 12. This argument is best specified
#'   when < 1 year of forecasts, or only observed data, are desired.
#' @param window Integration period (`1, 3, 6, 12, 24, 35, 60`),
#' @param period Number of months separating panels.
#' @param forecasts Number of forecasts to plot.
#' @param rows Number of panel rows.
#' @param cols Number of panel columns.
#' @param figtype Figure type to produce (`panel`. `twitter`, `banner`).
#' @param target_height Size of final output in inches.
#' @param layer Metric to be plotted--see details.
#' @param outdir Character string output director path.
#' @param wms Address of wms server. Does not need to be altered.
#' @param wmstype WMS server type (`geoserver`, `mapserver`).
#' @param workspace Data workspace (`era5_cfsv2_1979_2018`, `WSIM_derived_V2`, or `gldas`).
#' @param logo Logical to add logo to output figure.
#' @param ... Additional arguments to be passed to `wsim-graphics.py`.
#'
#' @return PNG file with panel figure.
#' @export
#'
#' @examples wsimReports::wsim_panels(roi = "United States", layer = "T_ave_rp")
#' @details
#'
#' This function provides an R interface to the [`wsim-graphics.py`](https://gitlab.com/isciences/wsim/wsim-graphics/-/blob/master/wsim_graphics.py)
#' command line tool. Additional options can be viewed in the GitLab repository.
#'
#' ## Layers
#'
#' Available WSIM Metrics (layers) include:
#'
#' \describe{
#' \item{"Bt_RO_rp"}{Total blue water anomalies.}
#' \item{"Bt_RO_sum_rp"}{Total blue water anomalies.}
#' \item{"composite"}{Water anomalies.}
#' \item{"composite_adjusted"}{Water anomalies.}
#' \item{"electric_power_gross_loss"}{Electric power loss (gross).}
#' \item{"PETmE_rp"}{Potential minus actual evapotranspiration.}
#' \item{"PETmE_sum_rp"}{Potential minus actual evapotranspiration sum.}
#' \item{"Pr_rp"}{Precipitation.}
#' \item{"Pr_sum_rp"}{Precipitation sum.}
#' \item{"RO_mm_rp"}{Runoff.}
#' \item{"RO_mm_sum_rp"}{Runoff sum (mm).}
#' \item{"Snowpack_rp"}{Snowpack.}
#' \item{"T_rp"}{Temperature.}
#' \item{"T_ave_rp"}{Average temperature.}
#' \item{"Ws_rp"}{Soil moisture.}
#' \item{"Ws_ave_rp"}{Average soil moisture.}
#' }
#'
#' Note, not all `layer` & `workspace` combinations are valid--some will produce
#' python errors.
wsim_panels <-
  function(wsim_graphics_path = NULL,
           roi = NULL,
           data_version = NULL,
           start_month = NULL,
           window = NULL,
           period = NULL,
           forecasts = NULL,
           rows = NULL,
           cols = NULL,
           figtype = NULL,
           target_height = NULL,
           layer = NULL,
           outdir = NULL,
           wms = NULL,
           wmstype = NULL,
           workspace = NULL,
           logo = FALSE,
           ...) {

    stopifnot(layer %in% c("Bt_RO_rp", "Bt_RO_sum_rp",
                           "composite", "composite_adjusted",
                           "composite_deficit", "composite_adjusted_deficit",
                           "composite_surplus", "composite_adjusted_surplus",
                           "electric_power_gross_loss",
                           "PETmE_rp", "PETmE_sum_rp",
                           "Pr_rp", "Pr_sum_rp",
                           "RO_mm_rp", "RO_mm_sum_rp",
                           "Snowpack_rp",
                           "T_rp", "T_ave_rp",
                           "Ws_rp", "Ws_ave_rp"))

    if(is.null(wsim_graphics_path)) {wsim_graphics_path = "/home/CORP.ISCIENCES.COM/jbrinks/Documents/wsim-graphics/wsim_graphics.py"}
    if(is.null(roi)) {roi = "United States"}
    if(is.null(data_version)) {data_version = format(Sys.Date() - lubridate::days(45), "%Y%m")}
    if(is.null(window)) {window = 12}
    if(is.null(period)) {period = 12 }
    if(is.null(rows)) {rows = 2}
    if(is.null(cols)) {cols = 2}
    if(is.null(figtype)) {figtype = "banner"}
    if(is.null(target_height)) {target_height = 4.25}
    if(is.null(layer)) {layer = "composite_adjusted"}
    if(is.null(outdir)) {outdir = paste0(getwd(),"/wsim-panels-output/")}
    if(is.null(wms)) {wms = "http://192.168.100.50:9034/wsim_wms/wms?"}
    if(is.null(wmstype)) {wmstype = "mapserver"}
    if(is.null(workspace)) {workspace = "WSIM_derived_V2"}
    if(!logo){logo = '--no-logo'} else {logo = NULL}

    arguments<-c(
      paste('--dataversion', data_version),
      paste('--roi ',"'",roi,"'", sep = ""),
      paste('--window',window),
      paste('--period', period),
      paste('--forecasts', forecasts),
      paste('--rows', rows),
      paste('--cols', cols),
      paste('--figtype', figtype),
      paste('--layer',layer),
      paste('--target_height',target_height),
      paste('--outdir',outdir),
      paste('--wms',wms),
      paste('--wmstype',wmstype),
      paste('--workspace',workspace),
      paste(logo),
      paste(...))

    if(!is.null(start_month)){arguments<-c(arguments,paste('--startmon', start_month))}

    command <- "python3"

    system2(command, args = c(wsim_graphics_path, arguments))
  }
