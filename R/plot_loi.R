#' Plot a LOI Time Series
#'
#' Given a latitude/longitude point location, plot a pixel time series figure
#' for a given WSIM layer/metric.
#'
#' @param locations_df A `data.frame` or `data.table` with column names that
#'   include `location_name`, `latitude`, and `longitude`. Although one can
#'   pass a multi-row `data.frame` to the function, only the first row will
#'   be used, therefore, the function must be looped through each row if the
#'   user wishes to retrieve figures for multiple locations.
#' @param wsim_workspace Name of WSIM workspace to us. Current available options
#'   are `era5_cfsv2_1979_2018` and `WSIM_derived_V2`.
#' @param data_version Character string of WSIM data version (`YYYYMM`).
#' @param start_date First date to plot (`YYYYMM`).
#' @param distribution Statistical distribution to establish deviance.
#' @param baseline_start_year Start year to set baseline comparison range.
#' @param baseline_stop_year End year to set baseline comparison range.
#' @param leg_logo Logical to plot corporate logo and legend?
#' @param layer WSIM layer/metric of interest. See Details below.
#' @param integration_months Integer with desired integration window.
#' @param layer_units Unit type to plot on y-axis; options include `"rp"`
#'   (Return Period) or `"sci"` (Scientific Units).
#' @param no_fits Logical to include (`FALSE`) or exclude (`TRUE`) the return
#' period overlay on.
#'
#' @return A list with 2 elements:
#'
#'   1. A [ggplot2::ggplot2()] device with time series (`loi_plot`).
#'   2. A [data.table::data.table()] of plotting data (`loi_dat`).
#'
#' @export
#'
#' @details
#' # Available Layers
#'
#' Current (03-2023) available `layers` include:
#'
#' \describe{
#' \item{Ws}{Soil Moisture Level (mm / m)}
#' \item{Pr}{Average Precipitation (mm / day)}
#' \item{P_net}{Net Precipitation (mm)}
#' \item{T}{Temperature}
#' \item{RO_mm}{Runoff (mm)}
#' \item{PETmE}{Potential Minus Actual Evapotranspiration (mm)}
#' \item{Bt_RO}{Total Blue Water (m^3)}
#' }
#'
#' # Development Background
#'
#' This function was originally developed inside of the
#'   `/vignettes/point_anom_timeseries.Rmd` vignette. All embedded functions and
#'   code were extracted individually and documented in January-February 2023 to
#'   develop an automated pipeline for developing dynamic country studies.
#'   Child/nested functions originally present in the vignette currently reside
#'   within  `/R/point_anom_ts_utils.R`.
#'
#'   Nomenclature and visual style for [plot_loi_ts()] and [plot_pop_frac_ts()]
#'   were streamlined to create symmetry across marketable products. This entire
#'   process was carried out by Josh Brinks (I), however, I (Josh Brinks) lack a
#'   greater understanding of the underlying WSIM framework--so certain
#'   inneficiences may still exist.
#'
plot_loi<-function(locations_df, wsim_workspace, layer, integration_months, layer_units,
                   data_version = NULL, start_date = NULL, distribution = NULL,
                   baseline_start_year = NULL, baseline_stop_year = NULL,
                   leg_logo = TRUE, no_fits = FALSE){
  # args setup----
  if(is.null(locations_df) | nrow(locations_df)<1){stop("POI locations_df may not be NULL or nrow < 1")}
  if(is.null(data_version)){data_version = format(Sys.Date() - lubridate::days(45), "%Y%m")}
  if(is.null(distribution)){distribution = 'gev'}
  if(is.null(start_date)){start_date = '201901'}
  if(is.null(baseline_start_year)){baseline_start_year = 1979}
  if(is.null(baseline_stop_year)){baseline_stop_year = 2018}

  library(magrittr)

  start_date <- as.character(start_date)
  baseline_start_year = as.integer(baseline_start_year)
  baseline_stop_year = as.integer(baseline_stop_year)

  if(!(layer %in% c('Ws', 'Pr','P_net', 'T', 'RO_mm', 'PETmE', 'Bt_RO'))){
    stop("Invalid layer selection; layer must be one of: Ws, Pr, T, RO_mm, PETmE, Bt_RO")}

  if(!(layer_units %in% c('sci', 'rp'))){
    stop("Invalid layer_units selection; layer_units must be one of: 'sci', 'rp'")}

  stopifnot(wsim_workspace %in% c("era5_cfsv2_1979_2018","WSIM_derived_V2"))

  baseline_start_year <- wsimReports::wsim_workspace_paths[name == wsim_workspace, baseline_start]
  baseline_stop_year <- wsimReports::wsim_workspace_paths[name == wsim_workspace, baseline_stop]

  # locate WSIM outputs----
  fcst_workspace <- wsimReports::load_wsim_workspace(
    wsimReports::wsim_workspace_paths[name == wsim_workspace, wsim_config],
    wsimReports::wsim_workspace_paths[name == wsim_workspace, wsim_source],
    wsimReports::wsim_workspace_paths[name == wsim_workspace, wsim_derived],
    distribution_subdir = wsimReports::wsim_workspace_paths[name == wsim_workspace, distribution_subdir],
    distribution = distribution,
    baseline_start_year = if(is.na(baseline_start_year)) NULL else as.integer(baseline_start_year),
    baseline_stop_year = if(is.na(baseline_stop_year)) NULL else as.integer(baseline_stop_year))

  # establish date ranges----
  yrmo_latest <- data_version
  date_latest <- lubridate::make_date(
    year = substr(yrmo_latest, 1, 4),
    month = substr(yrmo_latest, 5, 6),
    day = '01')

  yrmo_start <- start_date
  start_date <- lubridate::make_date(
    year = substr(start_date, 1, 4),
    month = substr(start_date, 5, 6),
    day = '01')

  require('lubridate') # to get the %m+% operator
  date_next <- date_latest + months(1)
  yrmo_next <- sprintf('%4d%02d', year(date_next), month(date_next))

  date_9monthsfcst <- date_latest %m+% months(9)
  yrmo_9monthsfcst <- sprintf('%4d%02d', year(date_9monthsfcst), month(date_9monthsfcst))

  obs_yearmons <- wsimReports::expand_dates(sprintf('[%s:%s]',yrmo_start,yrmo_latest))
  fcst_yearmons <- wsimReports::expand_dates(sprintf('[%s:%s]',yrmo_next,yrmo_9monthsfcst))

  # baseline fit data----
  ## select appropriate sum stat depending on layer and integration----
  if(integration_months > 1) {
    if (layer %in% c('T', 'Ws')) {fit_stat <- 'ave'} else{fit_stat <- 'sum'}
  } else {fit_stat <- NULL}

  ## apply through each month and identify paths to the baseline fit files----
  if(no_fits==FALSE) {
    fitpaths <- sapply(1:12, function(month_num) {
      fcst_workspace$fit_obs(
        var = layer,
        window = as.integer(integration_months),
        stat = if (exists("fit_stat"))
          fit_stat
        else
          NULL,
        month = month_num)})

    ## subset each baseline file for just the pixel containing our location----
    fit_pts <- wsimReports::read_and_subset_nc(
      locations = locations_df,
      fnames = fitpaths,
      fits = TRUE,
      varname = NULL)

    ## convert to baseline RP values from sci units and distribution parameters----
    fit_pts <- fit_pts %>%
      dplyr::rowwise() %>%
      dplyr::mutate(
        def_50 = lmom::quagev(0.020, para = c(location, scale, shape)),
        def_40 = lmom::quagev(0.025, para = c(location, scale, shape)),
        def_30 = lmom::quagev(0.015, para = c(location, scale, shape)),
        def_20 = lmom::quagev(0.05, para = c(location, scale, shape)),
        def_10 = lmom::quagev(0.10, para = c(location, scale, shape)),
        def_5 = lmom::quagev(0.20, para = c(location, scale, shape)),
        def_3 = lmom::quagev((1 / 3), para = c(location, scale, shape)),
        sur_3 = lmom::quagev((2 / 3), para = c(location, scale, shape)),
        sur_5 = lmom::quagev(0.80, para = c(location, scale, shape)),
        sur_10 = lmom::quagev(0.90, para = c(location, scale, shape)),
        sur_20 = lmom::quagev(0.95, para = c(location, scale, shape)),
        sur_30 = lmom::quagev(0.9625, para = c(location, scale, shape)),
        sur_40 = lmom::quagev(0.975, para = c(location, scale, shape)),
        sur_50 = lmom::quagev(0.98, para = c(location, scale, shape)))

    ## convert to long format and flip the sign on deficit values for plotting----
    fit_pts_long <- fit_pts %>%
      tidyr::pivot_longer(
        cols = tidyselect::matches("sur|def"),
        names_to = c('rp_type', 'rp_value'),
        names_sep = '_') %>%
      dplyr::mutate(
        return_period = as.numeric(rp_value) * (-1) ^ (rp_type == 'def'),
        rp_sci_value = value,
        .keep = 'unused')

    ## arranging and creating lagged values (not sure why lagging/leading)----
    fit_pts_long <- fit_pts_long %>%
      dplyr::arrange(location_name, month_num, return_period) %>%
      dplyr::group_by(location_name, month_num) %>%
      dplyr::mutate(
        rp_prior = dplyr::lag(return_period, n = 1L),
        rp_next = dplyr::lead(return_period, n = 1L),
        rp_sci_value_prior = dplyr::lag(rp_sci_value, n = 1L),
        rp_sci_value_next = dplyr::lead(rp_sci_value, n = 1L)) %>%
      dplyr::ungroup()}

  # observed and forcasted data----
  ## can get return periods of interest for any metric
  if(layer_units=='rp') {
    obs_fnms <- sapply(obs_yearmons,
                       function(yearmon) {
                         fcst_workspace$return_period(
                           yearmon = yearmon,
                           window = as.integer(integration_months),
                           target = NULL)})

    fcst_fnms <- sapply(fcst_yearmons,
                        function(yearmon) {
                          fcst_workspace$return_period_summary(
                            yearmon = yrmo_latest,
                            window = as.integer(integration_months),
                            target = yearmon)})}

  ## sci units; the forcing variables (T, Pr) are separate from derived
  ## which requires a different call to the wsim_workspace ($results)
  if(layer_units=='sci' & !(layer %in% c('T', 'Pr'))) {
    obs_fnms <- sapply(obs_yearmons,
                       function(yearmon) {
                         fcst_workspace$results(
                           yearmon = yearmon,
                           window = as.integer(integration_months),
                           target = NULL)})
    fcst_fnms <- sapply(fcst_yearmons,
                        function(yearmon) {
                          fcst_workspace$results(
                            yearmon = yrmo_latest,
                            window = as.integer(integration_months),
                            target = yearmon,
                            summary = TRUE)})}

  ## the forcing variables with scientific units are also in their own
  ## wsim_workspace object ($forcing and $forcing_summary for windows > 1)
  if(layer_units=='sci' & layer %in% c('T', 'Pr')) {
    obs_fnms <- sapply(obs_yearmons,
                       function(yearmon) {
                         fcst_workspace$forcing(
                           yearmon = yearmon,
                           window = as.integer(integration_months),
                           target = NULL)})
    fcst_fnms <- sapply(fcst_yearmons,
                        function(yearmon) {
                          fcst_workspace$forcing_summary(
                            yearmon = yrmo_latest,
                            window = as.integer(integration_months),
                            target = yearmon)})}

  # subset observed and forecasted for pixel with point locations Bt_RO for
  # integration > 1 has _min, _max, and _sum; other layers do not we have to
  # change the layer name with this combination of inputs (Bt_RO $ int >1)

  if (layer == "Bt_RO" &
      integration_months > 1) {
    layer_pts_layer <- "Bt_RO_sum"
  } else{layer_pts_layer <- layer}

  # run the extraction
  layer_pts <- wsimReports::read_and_subset_nc(locations = locations_df,
                                           fnames = c(obs_fnms, fcst_fnms),
                                           varname = layer_pts_layer)

  # plot them----
  ## what are the unique names, for now, it only will take the first location_df
  ## row
  poi_names<-unique(locations_df$location_name)

  dat <- layer_pts %>%
    dplyr::filter(location_name==poi_names[1])

  # merge the fits and observed/forecasted data together

if(no_fits == FALSE){
  fit_dat <- dat %>%
    dplyr::select(x, y, location_name, dateobj, month) %>%
    dplyr::distinct() %>%
    dplyr::mutate(month_num = month,
                  .keep ='unused') %>%
    dplyr::left_join(fit_pts_long)

  dat <- dat %>%
    dplyr::rowwise() %>%
    dplyr::mutate(
      rp_value = wsim.distributions::quantile2rp(
        wsim.distributions::cdfgev(
          x = value,
          location = wsimReports::extract_fitdat(location_name, month, fit_dat)[['location']],
          scale = wsimReports::extract_fitdat(location_name, month, fit_dat)[['scale']],
          shape = wsimReports::extract_fitdat(location_name, month, fit_dat)[['shape']])),
      outlier = abs(rp_value) >= 50
    ) %>%
    dplyr::ungroup()}

  poi_plot <- wsimReports::plot_point_timeseries(
    var_name = layer,
    int_period = integration_months,
    unit_type = layer_units,
    fit_dat = if(no_fits) NULL else fit_dat,
    dat,
    leg_logo,
    no_fits)

  layer_labs <- data.table::data.table(
    layers = c('Ws',
               'Pr',
               'P_net',
               'T',
               'RO_mm',
               'PETmE',
               'Bt_RO'),
    desc = c(
      "Available Soil Moisture",
      "Average Precipitation",
      "Net Precipitation",
      "Average Temperature",
      "Runoff",
      "Potential Minus Actual Evapotranspiration",
      "Total Blue Water"),
    units = c("(mm m^-1)",
              "(mm Day^-1)",
              "(mm)",
              "(degree C)",
              "(mm)",
              "(mm)",
              "(m^3)"))

  layer_description <- layer_labs$desc[layer_labs$layers==layer]
  layer_units_desc <- layer_labs$units[layer_labs$layers==layer]

  if(layer_units == 'sci'){
    poi_plot <- poi_plot +
      ggplot2::labs(
        title = paste0(unique(dat$location_name[1]), ": ",layer_description),
        subtitle = paste0("Observed and Forecasted Values Integrated Over a ",
                          integration_months,
                          "-Month Integration Period*"))}

  if(layer_units == 'rp'){
    poi_plot <- poi_plot +
      ggplot2::labs(
        title = paste0(unique(dat$location_name[1]), ": ",layer_description," Anomalies"),
        subtitle = paste0("Observed and Forecasted Return Periods Integrated Over a ",
                          integration_months,
                          "-Month Integration*"))}

  workspace_label<-wsimReports::wsim_workspace_paths[name==wsim_workspace, pretty_name]
  poi_plot <- poi_plot +
    ggplot2::labs(x='',
                  caption = glue::glue("*Solid line depicts observed values,",
                                       " grey line(s) indicates forecasted values with interquartile",
                                       " intervals (dashed), using the {workspace_label} model."))+
    ggplot2::ylab(ifelse(layer_units=='sci',
                         parse(text = paste(gsub(x = layer_description, pattern = " ", replacement = "~"),
                                            gsub(x = layer_units_desc, pattern = " ", replacement = "~"),
                                            sep ="~")),
                         'Return Period (yrs)'))

  library(ggplot2)
  ## Y-Axis Transformations

    if (layer == "Bt_RO" & layer_units == 'sci'){
    if(integration_months %in% c(1,3)) {
      poi_plot <- poi_plot + ggplot2::scale_y_continuous(
        labels = function(x)x / 100000)

      poi_plot <- poi_plot + ggplot2::labs(y = parse(text = paste(
        gsub(
          x = "Total Blue Water (Hundred Thousands m^3)",
          pattern = " ",
          replacement = "~"))))}

    if(integration_months > 9) {
      poi_plot <- poi_plot + ggplot2::scale_y_continuous(
        labels = function(x)x / 1000000)

      poi_plot <- poi_plot + ggplot2::labs(y = parse(text = paste(
        gsub(
          x = "Total Blue Water (Millions m^3)",
          pattern = " ",
          replacement = "~"))))}}

  if(layer=="T" & layer_units == 'sci' & wsim_workspace == "era5_cfsv2_1979_2018"){
    poi_plot <- poi_plot + ggplot2::scale_y_continuous(
      labels = function(x)round(x -273.15))
  }


  loi_out<-list()
  loi_out$loi_plot<-poi_plot
  loi_out$loi_dat<-dat

  loi_out

}
