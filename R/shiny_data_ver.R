#' Shiny Data Version List
#' 
#' Create a list of available WSIM data versions by checking for
#' the presence of ERA5 and WSIM Derived V2 results .nc files.
#'
#' @return A character vector of formatted available WSIM data versions.
#' @export
#'
#' @examples data_ver_updated <- wsimReports::shiny_data_ver()
shiny_data_ver<-function(){

    data_ver_list_update <- seq.Date(
      from = Sys.Date() %m-% months(12),
      to = Sys.Date() %m-% months(2),
      by = "month")

    data_ver_list_update <- format(data_ver_list_update, "%B, %Y")

    ## check if the most recent run is complete
    fig_root <- file.path("/var/lib/jenkins/mnt/fig")
    era5_workspace <- file.path("WSIM_DEV/WSIM_ERA5/derived_era5/gev_1979_2018/rp")
    wsim_derived_V2_workspace <- file.path("WSIM/WSIM_derived_V2/rp")

    latest_run <- Sys.Date() %m-% months(1)
    latest_run <- format(latest_run, "%Y%m")
    latest_run_file <- paste0("rp_1mo_", latest_run, ".nc")

    run_complete <-
      file.exists(
        file.path(fig_root, era5_workspace, latest_run_file),
        file.path(fig_root, wsim_derived_V2_workspace, latest_run_file))

    if (all(run_complete)) {
      data_ver_list_update <-
        c(data_ver_list_update, format(lubridate::ym(latest_run), "%B, %Y"))
    }
    
    data_ver_list_update
}
