
pop_exposure <-
  bslib::nav(
    "Pop-Exposure",
    fluidPage(
      fluidRow(
        column(
          12,
          align = 'center',
          style = "padding-top: 75px; padding-bottom: 0px;",
          img(src = "pop_exp_logo.svg", width = 500)
        )),
      hr(),
      fluidRow(tags$style(HTML("
                           .card-header {
    padding: var(--bs-card-cap-padding-y) var(--bs-card-cap-padding-x);
    margin-bottom: 0;
    color: #3398cb;
    text-align: center;
    font-weight: bold;
    font-family: Arial;
    background-color: var(--bs-card-cap-bg);
    border-bottom: var(--bs-card-border-width) solid var(--bs-card-border-color);}
                           ")),
    column(1),
    column(10,
           bslib::card(
             full_screen = FALSE,
             bslib::card_header("Fractional Population Exposure"),
             bslib::card_body(
               markdown("
                        The Fractional Population Exposure module presents a user friendly interface to the [plot_pop_frac_ts](https://isciences.gitlab.io/wsimReports/reference/plot_pop_frac_ts.html) R function.

                        * This module illustrates a time series of the percent of the population facing a particular composite adjusted anomaly.
                        * You may select any ISO Country or State integrated over 3 windows (1, 3, and 12 month).
                        * Population figures are derived from the Gridded Population of the World V4 2020 estimates.
                        ")
             )
           ))),
    hr(style = "color: #3398cb; size: 2px"),
    fluidRow(
             column(
               12,
               align = 'center',
               h3(style = "color: #3398cb; text-align: center; font-weight: bold; font-style: italic; font-family: Arial",
                  "Setup")
             )),
    fluidRow(
      column(3),
      column(3, align = 'center',
             selectInput("adm0", "ADMIN 0", sort(
               unique(wsimReports::adm0_adm1_lookup$ADM0NAME)
             ))),
      column(3, align = 'center',
             selectInput("adm1", "ADMIN 1", choices = NULL)),
      column(3)
    ),
    fluidRow(column(
      12,
      align = 'center',
      selectInput("data_ver_pop_frac", "Data Version", rev(data_ver_list))
    )),
    fluidRow(column(
      12,
      align = 'center',
      radioButtons(
        inputId = "pop_integration",
        label = "Integration Window",
        choices = c(
          "1-Month" = 1,
          "3-Month" = 3,
          "12-Month" = 12
        ),
        selected = 1,
        inline = TRUE
      )
    )),
    fluidRow(column(
      12,
      align = 'center',
      sliderInput(
        "plot_range",
        label = "Plotting Range",
        min = 1980,
        max = lubridate::year(Sys.Date()),
        value = c(2000, lubridate::year(Sys.Date())),
        sep = ""
      )
    )),
    hr(),
    h3(style = "color: #3398cb; text-align: center; font-weight: bold; font-style: italic; font-family: Arial",
       "Deliverables"),
    fluidRow(
      shinyjs::useShinyjs(),
      column(3),
      column(3, align = 'center',
             actionButton("execute_pop_frac", "Generate Figure")),
      column(3, align = 'center',
             actionButton("zip_pop_frac", "Zip All Images")),
      column(3)),
    fluidRow(column(12,
                    align = 'center', style = "padding: 30px",
                    downloadButton('download_pop_frac_zip', 'Download All Images'))),
    fluidRow(column(12, align = 'center',
    p("Please allow 15-45 seconds for your plot to generate depending on the input selections."))),
    hr(),
    fluidRow(column(12, align = 'center',
    h3(style = "color: #3398cb; text-align: center; font-weight: bold; font-style: italic; font-family: Arial",
       "Plot Preview"))),
  fluidRow(column(
    12,
    align = 'center',
    shinycssloaders::withSpinner(
      type = 6,
      color = "#ed5533",
      hide.ui = FALSE,
      plotOutput("pop_frac_plot")
    )
  )),
  fluidRow(column(12,
                  align = 'center',
                  uiOutput("download_pop_frac"))),
  hr(),
  fluidRow(column(12, style = "padding-bottom: 15px;",
                  htmlOutput("pop_frac_tab_title"))),
  fluidRow(column(2),
           column(8, align = 'center',
                  DT::DTOutput("pop_frac_table")),
           column(2))
  ))
