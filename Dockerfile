# Sycamore's Current Status (06/23)
FROM rocker/shiny:3.6.3

# Ubuntu libraries for R packages
RUN apt-get --allow-releaseinfo-change update && apt-get install -y \ 
  curl \
  gdebi-core \
  wget \
  libcurl4-openssl-dev \
  libssl-dev \
  libpng-dev \
  libgdal-dev \
  libnetcdf-dev \
  libmagick++-dev \
  libudunits2-dev \
  python3 \
  git


# grab wsim-graphics
ARG GITLAB_USER
ARG GITLAB_PASS

# copy necessary files
## app folder
COPY /inst/wsim_reports ./wsim_reports
## renv.lock file
COPY /renv.lock ./wsim_reports/renv.lock
WORKDIR ./wsim_reports

RUN Rscript -e "install.packages('remotes', repos = c(CRAN = 'https://cloud.r-project.org'))"
RUN Rscript -e "remotes::install_github('rstudio/renv')"
RUN Rscript -e "renv::isolate()"
RUN Rscript -e 'renv::restore()'

RUN Rscript -e "remotes::install_gitlab('isciences/wsimReports', auth_token = '$GITLAB_PASS', dependencies = FALSE)"

RUN git clone https://$GITLAB_USER:$GITLAB_PASS@gitlab.com/isciences/wsim/wsim-graphics.git

RUN mv /wsim_reports /srv/shiny-server/
# run app on container start
## expose port
EXPOSE 3838
CMD ["/usr/bin/shiny-server"]
# CMD ["R", "-e", "shiny::runApp('/wsim_reports', host = '0.0.0.0', port = 3838)"]