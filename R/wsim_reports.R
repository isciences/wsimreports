#' WSIM Reports
#' 
#' Launch the WSIM Reports Shiny Portal
#'
#' @return A shiny app.
#' @export
#'
#' @examples wsimReports::wsim_reports()
wsim_reports<-function(){
  
  shiny::runApp(system.file("wsim_reports/app.R", package = "wsimReports"))
  
}