loi <-
  bslib::nav(
    "LOI",
    fluidPage(
      fluidRow(
        column(
          12,
          align = 'center',
          style = "padding-top: 75px; padding-bottom: 0px;",
          img(src = "loi_logo.svg", width = 500))),
      hr(),
      fluidRow(tags$style(HTML("
                           .card-header {
    padding: var(--bs-card-cap-padding-y) var(--bs-card-cap-padding-x);
    margin-bottom: 0;
    color: #3398cb;
    text-align: center;
    font-weight: bold;
    font-family: Arial;
    background-color: var(--bs-card-cap-bg);
    border-bottom: var(--bs-card-border-width) solid var(--bs-card-border-color);}
                           ")),
    column(1),
    column(10,
           bslib::card(
             full_screen = FALSE,
             bslib::card_header("Location of Interest Analysis"),
             bslib::card_body(
               markdown("
                        The Location of Interest module presents a user friendly interface to the [plot_loi](https://isciences.gitlab.io/wsimReports/reference/plot_loi.html) R function.

                        * This module illustrates a time series for a user defined WSIM layer/metric in reference to a historic baseline specific to the selected WSIM Workspace.
                        * You may use the map to select any location, or enter the longitude and latitude directly.
                        * Figures may be generated using either:
                            + *Return Period:* a relative measure that relates a particular layer/metric to a historical baseline. This is a measure of rarity or frequency displayed as the number of years one would expect before an event of this magnitude returns.
                            + *Scientific Units:* the absolute observed or forecasted measurement for the given layer/metric; e.g. mm of water, temperature in celsius, etc.
                        * In locations where a historic baseline can not be established, users may use the _RP_ _Overlay_ switch to remove the return period (RP) reference and simply plot the observed time series.
                        ")
             )
           ))),
    hr(style = "color: #3398cb; size: 2px"),
      fluidRow(column(
        12,
        align = 'center',
        h3(style = "color: #3398cb; text-align: center; font-weight: bold; font-style: italic; font-family: Arial",
           "Setup"))),
      fluidRow(column(
        12,
        align = 'center',
        selectInput("data_ver_loi", "Data Version", rev(data_ver_list)))),
      bslib::card(
        fluidRow(column(
        12,
        align = 'center',
        radioButtons(
          inputId = "loi_metric",
          label = "WSIM Layer",
          choices = c(
            "Total Blue Water" = "Bt_RO",
            "Temperature" = "T",
            "Soil Moisture" = "Ws",
            "Average Daily Precipitation" = "Pr",
            "Net Precipitation" = "P_net",
            "PETmE" = "PETmE",
            "Runoff" = "RO_mm"),
          selected = "Bt_RO",
          inline = TRUE)))),
      fluidRow(column(
        12,
        align = 'center',
        radioButtons(
          inputId = "workspace_loi",
          label = "WSIM Workspace",
          choices = c("ERA5" = "era5_cfsv2_1979_2018",
                      "WSIM Derived V2" = "WSIM_derived_V2"
                   #   "WSIM-GLDAS (2.0)" = "gldas"
                      ),
          inline = TRUE
        ))),
      fluidRow(
        column(
          4,
          align = 'center',
          radioButtons(
            inputId = "loi_units",
            label = "Plotting Units",
            choices = c("Return Period" = "rp",
                        "Scientific" = "sci"),
            selected = "rp",
            inline = TRUE)),
        column(
          4,
          align = 'center',
          radioButtons(
            inputId = "integration",
            label = "Integration Window",
            choices = c(
              "1-Month" = 1,
              "3-Month" = 3,
              "12-Month" = 12),
            selected = 1,
            inline = TRUE)),
         column(4,
                align = 'center',
                shinyWidgets::switchInput(
                  inputId = "loi_no_fits",
                  label = "RP Overlay",
                  value = TRUE))),
      h5(style = "font-weight: bold; text-align: center;", "Plotting Start Year and Month"),
      fluidRow(
        column(3),
        column(
          3,
          align = 'center',
          selectInput(
            "loi_start_year",
            "Year",
            selected = 2019,
            choices = seq(2022, 2000))),
        column(
          3,
          align = 'center',
          selectInput(
            "loi_start_month",
            "Month",
            selected = "January",
            choices = stats::setNames(object = sprintf("%02d", seq(1, 12)),
                                      nm = month.name))),
        column(3)),
      hr(),
      h3(style = "color: #3398cb; text-align: center; font-weight: bold; font-style: italic; font-family: Arial",
         "Select Location"),
      fluidRow(column(
        12,
        align = 'center',
        style = "padding: 10px",
        p("Click the map to select a location."))),
      fluidRow(
        column(1),
        column(10, style = "border: 3px; border-color: #60686c; border-style: solid; padding: 0px;",
               leaflet::leafletOutput("loi_map")),
        column(1)),
      fluidRow(
        column(3),
        column(2, align = 'center', style = "padding-top: 15px", textInput("loi_lat", "Latitude")),
        column(2, align = 'center', style = "padding-top: 15px", textInput("loi_long", "Longitude")),
        column(2, align = 'center', style = "padding-top: 15px", textInput("loi_name", "Place Name",
                    placeholder = "Enter a Label")),
        column(3)),
      fluidRow(column(
        12,
        align = 'center',
        h5(style = "font-weight: bold; text-align: center;", "Generate Figure"),
        shinyjs::useShinyjs(),
        actionButton("execute_loi", "Go!", style = "color: #fff; background-color: #ed5533D9;"))),
      fluidRow(column(
        12,
        align = 'center',
        style = "padding-top: 20px",
        p("Please allow 1-2 minutes for your plot to generate depending on the input selections."))),
      hr(),
      fluidRow(column(
        12,
        align = 'center',
        shinycssloaders::withSpinner(
          type = 6,
          color = "#ed5533",
          hide.ui = TRUE,
          plotOutput("loi_plot")))),
      fluidRow(
        column(
          12,
          style = "padding-bottom: 15px;",
          align = 'center',
          uiOutput("download_loi"))),
      hr(),
      fluidRow(column(12, align = 'center',
                      htmlOutput("loi_tab_title"))),
      fluidRow(column(1),
               column(10, align = 'center',
                      DT::DTOutput("loi_table")),
               column(1)))
    )

